/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pointinspace;

/**
 *
 * @author wkroos
 */
public class SpaceTester {
    public static void main(String[] args) {
        // TODO code application logic here
        SpaceTester mainObject = new SpaceTester();
        mainObject.start();
    }

    void start() {
        PointInSpace p1 = new PointInSpace();
        p1.x = 1;
        p1.y = 2;
        p1.z = 3;
        PointInSpace p2 = new PointInSpace();
        p2.x = 4;
        p2.y = 5;
        p2.z = 6;
        System.out.println("p2 = " + p2.calculateDistance(p1));
        System.out.println("p2 = " + p2);
        System.out.println("p1 = " + p1);
    }

}

//https://bitbucket.org/minoba/javaintroprogrammingassignments/src
